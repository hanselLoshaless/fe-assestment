# Library/Framework Used
## Server
1. JSON Server
## Client
1. Bootstrap for grid, layouting, and Box Card.
2. Vue, Vue CLI
<br/>
<br/>
# WEBSITE
## Page
1. Home
    ```
    http://localhost:8080/
    ```
2. Product List
    ```
    http://localhost:8080/category/:type
    ```
3. Error 404
<br/>
<br/>
## Components
1. Banner => Banner image at Home and **_Product List_** Page
2. MainProduct => Main Product Card used at **_Home_** Page
3. SubProduct => Sub Product Card used at **_Home_** Page
4. FilterOption => Option list used at **_Product List_** page
5. TableData => Table Row Data used at **_Product List_** page