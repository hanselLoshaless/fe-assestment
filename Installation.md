# Server Installation
type this code on ./server
```
node server.js
```
server will run on http://localhost:3000/ by default
<br />
<br />
<br />
# Client Installation
type this code on ./client
```
npm run serve
```
server will run on http://localhost:8080/ by default