# Investree App endpoint
Investree is a Market Place lending

## End Point
1. GET /main
2. GET /invoice
3. GET /conventional_invoice
4. GET /sharia_invoice
5. GET /osf
6. GET /conventional_osf
7. GET /sharia_osf
8. GET /reksadana
9. GET /sbn

<br/>

 ## **GET /main**
> get all main Products

_Response (200)_
```
{
  "contents: : 
  [
    {
      "name": "<string>",
      "count": "<integer>",
      "sub": "<string>"
    },
  ]
}
```
 ## **GET /invoice**
> get all Invoice Products

_Response (200)_
```
{
  "contents: : 
  [
    {
      "name": "<string>",
      "count": "<integer>",
      "sub": "<string>"
    },
  ]
}
```
 ## **GET /conventional_invoice**
> get all Conventional Invoice Products

_Response (200)_
```
{
  "contents: : 
  [
    {
      "name": "<string>",
      "amount": "<integer>",
      "tenor": "<integer>",
      "grade": "<string>",
      "rate": "<integer>",
      "sub": null,
    },
  ]
}
```
 ## **GET /sharia_invoice**
> get all Sharia Invoice Products

_Response (200)_
```
{
  "contents: : 
  [
    {
      "name": "<string>",
      "amount": "<integer>",
      "tenor": "<integer>",
      "grade": "<string>",
      "rate": "<integer>",
      "sub": null,
    },
  ]
}
```
 ## **GET /osf**
> get all OSF Products

_Response (200)_
```
{
  "contents: : 
  [
    {
      "name": "<string>",
      "count": "<integer>",
      "sub": "<string>"
    },
  ]
}
```
 ## **GET /conventional_osf**
> get all Conventional OSF Products

_Response (200)_
```
{
  "contents: : 
  [
    {
      "name": "<string>",
      "amount": "<integer>",
      "tenor": "<integer>",
      "grade": "<string>",
      "rate": "<integer>",
      "sub": null,
    },
  ]
}
```
 ## **GET /sharia_osf**
> get all Sharia OSF Products

_Response (200)_
```
{
  "contents: : 
  [
    {
      "name": "<string>",
      "amount": "<integer>",
      "tenor": "<integer>",
      "grade": "<string>",
      "rate": "<integer>",
      "sub": null,
    },
  ]
}
```
 ## **GET /sbn**
> get all SBN Products

_Response (200)_
```
{
  "contents: : 
  [
    {
      "name": "<string>",
      "amount": "<integer>",
      "tenor": "<integer>",
      "rate": "<integer>",
      "type": "<string>",
      "sub": null,
    },
  ]
}
```
 ## **GET /reksadana**
> get all REKSADANA Products

_Response (200)_
```
{
  "contents: : 
  [
    {
      "name": "<string>",
      "amount": "<integer>",
      "tenor": "<integer>",
      "rate": "<integer>",
      "type": "<string>",
      "sub": null,
    },
  ]
}
```