const main  = require('./main.json');

const invoice = require('./invoice/invoice.json');
const conventional_invoice  = require('./invoice/conventional_invoice.json');
const sharia_invoice = require('./invoice/sharia_invoice.json');

const osf = require('./osf/osf.json')
const conventional_osf  = require('./osf/conventional_osf.json');
const sharia_osf = require('./osf/sharia_osf.json');

const reksadana  = require('./reksadana/reksadana.json');
const sbn = require('./sbn/sbn.json');


module.exports = function() {
return {
  main  : main,
  invoice : invoice,
  conventional_invoice  : conventional_invoice,
  sharia_invoice : sharia_invoice,
  osf : osf,
  conventional_osf  : conventional_osf,
  sharia_osf : sharia_osf,
  reksadana  : reksadana,
  sbn : sbn,
 }
}