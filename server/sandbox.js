let a = 
[
  {
    name: "SBR XXX",
    amount: 1000000,
    tenor: 120,
    rate: 7,
    type: "SBR",
    sub: null
  },
  {
    name: "SBR YYY",
    amount: 2000000,
    tenor: 120,
    rate: 8,
    type: "SBR",
    sub: null
  },
  {
    name: "SBR YYY",
    amount: 2000000,
    tenor: 120,
    rate: 8,
    type: "SBR",
    sub: null
  },
]

function filterObject(array) {
  let obj = {};
  for (let i = 0; i < array.length; i++) {
    if (!obj[array[i].rate]) {
      obj[array[i].rate] = array[i].rate
    }
  };
  return Object.values(obj)
}

console.log(filterObject(a));